﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class AccelButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler
{
    public Pino pino;
    public PinoController pinoController;
    public bool isDown;
    public bool isHover;

    public void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isHover = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isDown = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isHover = false;
    }

}
