﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class BoosterButton : MonoBehaviour, IPointerDownHandler 
{
    public StageManager stageManager;

    public void OnPointerDown(PointerEventData eventData)
    {
        stageManager.pinoController.Booster();
    }
}
