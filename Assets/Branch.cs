﻿using UnityEngine;
using System.Collections;

public class Branch : MonoBehaviour 
{
    public bool isSelectable;
    public BranchLocation branchLocation;
    public BranchType branchType;
    public Block parentBlock;
    public PinoPath pathLeft, pathStraight, pathRight;
    public GameObject signLeft, signStraight, signRight;
    
    public void EnableBranch()
    {
        isSelectable = true;
        signLeft.SetActive(true);
        signStraight.SetActive(true);
        signRight.SetActive(true);
        DrawDirectionArrow();
    }

    public void DisableBranch()
    {
            isSelectable = false;
            signLeft.SetActive(false);
            signStraight.SetActive(false);
            signRight.SetActive(false);
            DrawDirectionArrow();
    }

    public void DrawDirectionArrow()
    {
        signLeft.SetActive(false);
        signStraight.SetActive(false);
        signRight.SetActive(false);
        if(isSelectable)
        {
            if(branchType == BranchType.STRAIGHTANDLEFT)
            {
                signLeft.SetActive(true);
                signStraight.SetActive(true);
            }
            else if(branchType == BranchType.STRAIGHTANDRIGHT)
            {
                signStraight.SetActive(true);
                signRight.SetActive(true);
            }
        }
    }
}
