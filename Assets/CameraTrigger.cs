﻿using UnityEngine;
using System.Collections;

public class CameraTrigger : MonoBehaviour 
{
    public bool isRotationTrigger;
    public string direction;
    //public Vector3 targetRotation;
    //public Vector3 targetPivot;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("CameraFollow"))
        {
            if (isRotationTrigger)
            {
                float time = 2.0f;
                if (direction.Equals("right"))
                    iTween.RotateTo(other.gameObject, new Vector3(30.0f, 30.0f, 0), time);
                else if (direction.Equals("up"))
                    iTween.RotateTo(other.gameObject, new Vector3(30.0f, 300.0f, 0), time);
                else if (direction.Equals("left"))
                    iTween.RotateTo(other.gameObject, new Vector3(30.0f, 210.0f, 0), time);
                else if (direction.Equals("down"))
                    iTween.RotateTo(other.gameObject, new Vector3(30.0f, 120.0f, 0), time);
                


                //Transform pivot = other.gameObject.transform.Find("Pivot");
                //iTween.MoveTo(pivot.gameObject, iTween.Hash("position", pivotPosition, "isLocal", true, "time", 2.0f));
            }
        }
        
    }
}
