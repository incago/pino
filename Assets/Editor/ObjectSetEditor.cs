﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(ObjectSet))]
public class ObjectSetEditor : Editor
{
    ObjectSet objectSet;
    private List<Transform> childObjects = new List<Transform>();

    public void OnEnable()
    {
        objectSet = (ObjectSet)target;
        //SceneView.onSceneGUIDelegate = ObjectSetUpdate;
    }

    public override void OnInspectorGUI()
    {
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Align Objects", GUILayout.Width(255)))
        {
            Transform parentTransform = objectSet.GetComponent<Transform>();
            foreach(Transform child in parentTransform)
            {
                childObjects.Add(child);
            }
            foreach(Transform child in childObjects)
            {
                child.localPosition = new Vector3(Mathf.Round(child.localPosition.x),
                                                Mathf.Round(child.localPosition.y),
                                                Mathf.Round(child.localPosition.z));
            }
            childObjects.Clear();

            Debug.Log("Child objects align completed");
        }

        GUILayout.EndHorizontal();
        SceneView.RepaintAll();
    }

    void ObjectSetUpdate(SceneView sceneview)
    {
        Event e = Event.current;
    }
}
