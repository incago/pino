﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class GameOverPanel : MonoBehaviour, IPointerDownHandler
{
    public UIManager uiManager;

    public void OnPointerDown(PointerEventData eventData)
    {
        uiManager.gameOverAnimator.SetBool("isOn", false);
        uiManager.scoreAnimator.SetBool("isOn", true);
        Debug.Log("Incago");
        uiManager.SetCameraBlur(true);
    }
}
