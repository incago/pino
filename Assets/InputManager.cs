﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour
{
    private StageManager stageManager;
    public float minSwipeDistY;
    public float minSwipeDistX;
    private Vector2 startPos;
    public HorizontalMoveButton leftMoveButton, rightMoveButton;

    void Awake()
    {
        stageManager = GameObject.FindGameObjectWithTag("StageManager").GetComponent<StageManager>();
    }

    void DetectKey()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            stageManager.pino.AttempMove(-1);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            stageManager.pino.AttempMove(1);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Debug.Log("UpArrow");
            stageManager.pinoController.Jump();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            stageManager.pinoController.Booster();
        }
    }

    void Update()
    {
        #if UNITY_EDITOR
        DetectKey();
        #endif
        
        //button control
        DetectButton();
        //touch control
        DetectTouch();
    }

    void DetectButton()
    {
        if (leftMoveButton.isHover)
        {
            stageManager.pino.AttempMove(-1);
        }
        else if (rightMoveButton.isHover)
        {
            stageManager.pino.AttempMove(1);
        }
    }

    void DetectTouch()
    {
        if (Input.touchCount > 0)
        {
            for(int i = 0; i<Input.touchCount; i++)
            {
                if(Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    if(Input.GetTouch(i).tapCount == 2)
                    {
                        Debug.Log("Double Tap");
                        stageManager.pinoController.Jump();
                    }
                }
            }


            Touch touch = Input.touches[0];

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPos = touch.position;
                    break;

                case TouchPhase.Ended:
                    float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
                    Debug.Log("swipeDistVertical : " + swipeDistVertical);
                    if (swipeDistVertical > minSwipeDistY)
                    {
                        float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
                        /*
                        if (swipeValue > 0)//up swipe
                        {
                            //Jump ();
                            Debug.Log("Jump");
                            pinoController.Jump();
                        }
                        else
                         */
                        if (swipeValue < 0)//down swipe
                        {
                            //Shrink ();
                            Debug.Log("Shrink");
                            stageManager.pinoController.Booster();
                        }
                    }

                    float swipeDistHorizontal = (new Vector3(touch.position.x, 0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;
                    Debug.Log("swipeDistHorizontal: " + swipeDistHorizontal);
                    if (swipeDistHorizontal > minSwipeDistX)
                    {
                        float swipeValue = Mathf.Sign(touch.position.x - startPos.x);
                        if (swipeValue > 0)//right swipe
                        {
                            //MoveRight ();
                            Debug.Log("MoveRight");
                            stageManager.pino.AttempMove(1);
                        }
                        else if (swipeValue < 0)//left swipe
                        {
                            //MoveLeft ();
                            Debug.Log("MoveLeft");
                            stageManager.pino.AttempMove(-1);
                        }
                    }
                    break;
            }
        }
    }
}
