﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class JumpButton : MonoBehaviour, IPointerDownHandler 
{
    public StageManager stageManager;

    public void OnPointerDown(PointerEventData eventData)
    {
        stageManager.pinoController.Jump();
    }
}
