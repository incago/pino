﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LabelHandle : MonoBehaviour 
{
    public Text slideValueLabel;

    public void SetLabelHandleText(float value)
    {
        slideValueLabel.text = value.ToString("F1");
    }
	
}
