﻿using UnityEngine;
using System.Collections;

public class ObjectSet : MonoBehaviour 
{   
    void OnDrawGizmos()
    {
        Vector3 origin = transform.position;

        Gizmos.DrawWireCube(new Vector3(origin.x, origin.y - 0.5f, origin.z), new Vector3(100.0f, 1.0f, 100.0f));
        for (int i = 0; i < 5; i++)
        {
            Gizmos.DrawLine(new Vector3(origin.x - 30.0f, origin.y + 0.5f, origin.z - 31.0f - 2.0f * i), new Vector3(origin.x + 30.0f, origin.y + 0.5f, origin.z - 31.0f - 2.0f * i));
            //Gizmos.DrawLine(new Vector3(origin.x + 31.0f + 2.0f * i, origin.y + 0.5f, origin.z - 30.0f), new Vector3(origin.x + 31.0f + 2.0f * i, origin.y + 0.5f, origin.z + 30.0f));
        }
        for (int i = 0; i < 21; i++)
        {
            Gizmos.DrawLine(new Vector3(origin.x - 30.0f + 3.0f * i, origin.y + 0.5f, origin.z - 31.0f), new Vector3(origin.x - 30.0f + 3.0f * i, origin.y + 0.5f, origin.z - 39.0f));
            //Gizmos.DrawLine(new Vector3(origin.x + 31.0f, origin.y + 0.5f, origin.z - 30.0f + 3.0f * i), new Vector3(origin.x + 39.0f, origin.y + 0.5f, origin.z - 30.0f + 3.0f * i));
        }
    }
}
