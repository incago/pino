﻿using UnityEngine;
using System.Collections;

public class Pino : MonoBehaviour 
{
    public PinoController pinoController;
    //private UIManager uiManager;
    public Animator animator;
    public float animationSpeed;
    public GameObject body;

    public float moveTime = 0.3f;
    private bool isChangingTrack;
    private float inverseMoveTime;
    private float trackWidth;

    private int runningTrack;
    private int numberOfTrack;

    public bool isInvincible = false;

    void Awake()
    {
        animator.speed = animationSpeed;
        isChangingTrack = false;
        inverseMoveTime = 1f / moveTime;
        runningTrack = 3;
        numberOfTrack = 5;
        trackWidth = 2.0f;
    }

    public void AttempMove(int direction)
    {
        if (!isChangingTrack && !pinoController.isDead)
        {
            if ((runningTrack == 1 && direction == -1) || runningTrack == numberOfTrack && direction == 1)
            {
                Debug.Log("leftmost or rightmost track");
            }
            else
            {
                Move(direction);
            }
        }
    }

    public void Move(int direction)
    {
        float endPositionZ = GetDestZ(runningTrack + direction);
        StartCoroutine(SmoothMovement(direction, endPositionZ));
    }

    private IEnumerator SmoothMovement(int direction, float targetCordinate)
    {
        isChangingTrack = true;
        float remainDistance = targetCordinate - body.transform.localPosition.x;
        while (Mathf.Abs(remainDistance) > 0.01f)
        {
            Vector3 newPosition = Vector3.MoveTowards(body.transform.localPosition, new Vector3(targetCordinate, body.transform.localPosition.y, body.transform.localPosition.z), inverseMoveTime * Time.deltaTime);
            Vector3 movePosition = newPosition - body.transform.localPosition;
            body.transform.Translate(movePosition, Space.Self);
            remainDistance = targetCordinate - body.transform.localPosition.x;
            
            yield return null;
        }
        runningTrack = runningTrack + direction;
        isChangingTrack = false;
    }

    private float GetDestZ(int trackNumber)
    {
        float mostleftTrackZ;
        if (numberOfTrack % 2 == 0)
        {
            mostleftTrackZ = -trackWidth * (numberOfTrack / 2) + trackWidth / 2.0f + (trackNumber - 1) * trackWidth;
        }
        else
        {
            mostleftTrackZ = -trackWidth * (numberOfTrack / 2) + (trackNumber - 1) * trackWidth;
        }
        return mostleftTrackZ;
    }

    void OnTriggerEnter(Collider other)
    {
        if (!pinoController.isDead)
        {
            if (!isInvincible)
            {
                if (other.CompareTag("Vehicle"))
                {
                    pinoController.KilledByVehicle(other.gameObject);
                }
                else if (other.CompareTag("Obstacle"))
                {
                    pinoController.KilledByObstacle(other.gameObject);
                }
            }
        }
    }

    public void SetInvincible(bool _invincible)
    {
        isInvincible = _invincible;
    }
}
