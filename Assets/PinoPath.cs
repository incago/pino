﻿using UnityEngine;
using System.Collections;

public class PinoPath : MonoBehaviour
{
    public int direction;
    public PathType pathType;
    public Branch followBranch;
    public Transform[] data;
    public float pathLength;
    public PinoPath nextPath;

    void Awake()
    {
        Initialize();
    }

    void Initialize()
    {
        pathLength = iTween.PathLength(data);
    }

    void OnDrawGizmos()
    {
        if (data != null && data.Length > 1)
        {
            iTween.DrawPathGizmos(data, Color.blue);
        }
    }
}
