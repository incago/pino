﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Block : MonoBehaviour 
{
    private StageManager stageManager;

    public CameraTrigger cameraTrigger;

    public float pathLength;
    public PinoPath pinoPath;
    public VehiclePath vehiclePath;
    
    public GameObject objectBlock;
    public GameObject buildingBlock;
    public bool isVehicleActive = false;

    void Awake()
    {
        stageManager = GameObject.FindGameObjectWithTag("StageManager").GetComponent<StageManager>();
        Initialize();
    }
    
    public void Initialize()
    {
        InitVehicles();
        InitBuilding();
    }

    void InitVehicles()
    {
        int numberOfVehicle = Random.Range(3, 4);
        float vehicleSpeed = Random.Range(0.8f, 1.6f);
        float vehiclePositionUnit = 1.0f / numberOfVehicle;

        for(int indexOfVehicle = 0 ; indexOfVehicle < numberOfVehicle; indexOfVehicle ++)
        {
            GameObject targetGameObject = stageManager.vehiclePrefabs[Random.Range(0, stageManager.vehiclePrefabs.Length)];
            GameObject vehicleGameObject = (GameObject)Instantiate(targetGameObject, iTween.PointOnPath(vehiclePath.data, indexOfVehicle * vehiclePositionUnit), Quaternion.identity);
            vehicleGameObject.name = targetGameObject.name;
            vehicleGameObject.GetComponent<VehicleController>().Initialize(GetComponent<Block>(), vehiclePath, indexOfVehicle * vehiclePositionUnit, vehicleSpeed);
            vehicleGameObject.transform.SetParent(transform);
        }
    } 

    void InitBuilding()
    {
        GameObject buildingGameObject = Instantiate(stageManager.buildingBlockPrefabs[Random.Range(0, stageManager.buildingBlockPrefabs.Length)], transform.position, Quaternion.identity) as GameObject;
        buildingGameObject.transform.SetParent(transform);
    }
}
