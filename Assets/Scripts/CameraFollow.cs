﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System.Collections;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class CameraFollow : MonoBehaviour
{
    private StageManager stageManager;
    private Camera cam;
    private BlurOptimized blurOptimized;
    public Transform player;
    public Transform pivot;
    
    void Awake()
    {
        stageManager = GameObject.FindGameObjectWithTag("StageManager").GetComponent<StageManager>();
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        blurOptimized = cam.GetComponent<BlurOptimized>();
    }

    void Start()
    {
        player = stageManager.pino.transform;
    }

    public void Initialize(Vector3 cameraPosition, Vector3 cameraRotation, Vector3 pivotPosition)
    {
        transform.position = cameraPosition;
        transform.rotation = Quaternion.Euler(cameraRotation);
        pivot.transform.localPosition = pivotPosition; 
    }

    void LateUpdate()
    {
        if (!stageManager.pinoController.isDead)
            TrackPlayer();
    }

    public void ZoomTo(float zoomSize, float time)
    {
        StartCoroutine(ZoomToCo(cam.orthographicSize, zoomSize, time));
    }

    IEnumerator ZoomToCo(float currentZoomSize, float targetZoomSize, float time)
    {
        float t = 0.0f;
        while(t < 1.0f)
        {
            t += Time.deltaTime * (Time.timeScale / time);
            float calculatedSize = Mathf.Lerp(currentZoomSize, targetZoomSize, t);
            cam.orthographicSize = calculatedSize;
            yield return 0;
        }
    }
    
    public void PivotTo(Vector3 pivotPosition, float time)
    {
        StartCoroutine(PivotToCo(pivot.localPosition, pivotPosition, time));
    }

    IEnumerator PivotToCo(Vector3 currentPivotPosition, Vector3 targetPivotPosition, float time)
    {
        float t = 0.0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime * (Time.timeScale / time);
            Vector3 calculatedPosition =  Vector3.Lerp(currentPivotPosition, targetPivotPosition, t);
            pivot.localPosition = calculatedPosition;
            yield return 0;
        }
    }
    
    void TrackPlayer()
    {
        float targetX = player.position.x;
        float targetZ = player.position.z;

        transform.position = new Vector3(targetX, transform.position.y, targetZ);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="currentDirection">currentPath's direction</param>
    /// <param name="turnDirection">desire turn direction</param>
    public void RotateCamera(int currentDirection, int turnDirection, float time = 2.0f)
    {
        if ((currentDirection == 1 && turnDirection == -1) || (currentDirection == 3 && turnDirection == 1))
            iTween.RotateTo(gameObject, new Vector3(45.0f, 315.0f, 0), time);
        else if ((currentDirection == 2 && turnDirection == -1) || (currentDirection == 4 && turnDirection == 1))
            iTween.RotateTo(gameObject, new Vector3(45.0f, 225.0f, 0), time);
        else if ((currentDirection == 3 && turnDirection == -1) || (currentDirection == 1 && turnDirection == 1))
            iTween.RotateTo(gameObject, new Vector3(45.0f, 135.0f, 0), time);
        else if ((currentDirection == 4 && turnDirection == -1) || (currentDirection == 2 && turnDirection == 1))
            iTween.RotateTo(gameObject, new Vector3(30.0f, 30.0f, 0), time);
    }
    
    public void StartCameraWork(float time = 2.0f)
    {
        ZoomTo(12.0f, time);
        PivotTo(new Vector3(10.0f, 8.0f, 0), time);
        RotateCamera(2, 1, time);
    }

    public void SetCameraBlur(bool _active)
    {
        float time = 2.0f;
        StartCoroutine(SetCameraBlurCo(_active, time));
    }

    IEnumerator SetCameraBlurCo(bool _active, float time = 1.0f)
    {
        if (_active) blurOptimized.enabled = true;
        float currentBlurSize = _active ? 0.0f : 3.0f;
        float targetBlurSize = _active ? 3.0f : 0.0f;
        float t = 0.0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime * (Time.timeScale / time);
            float calculatedBlurSize = Mathf.Lerp(currentBlurSize, targetBlurSize, t);
            blurOptimized.blurSize = calculatedBlurSize;
            yield return 0;
        }
        if (!_active) blurOptimized.enabled = false;
    }
}
