﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class CameraNode
{
    public bool isRotateNode;
    public bool isZoomNode;
    public Vector3 position, rotation;
    public float size;
}
