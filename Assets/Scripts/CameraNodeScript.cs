﻿using UnityEngine;
using System.Collections;

public class CameraNodeScript : MonoBehaviour
{
    public bool isPositionNode;
    public Vector3 targetPosition;

    public bool isSpeedNode;
    public float targetSpeed;

    public bool isRotationNode;
    public Vector3 targetRotation;
    public Vector3 pivotPosition;

    public bool isZoomNode;
    public float targetSize;


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        float gizmoRadius = 1.00f;
        Gizmos.DrawSphere(transform.position, gizmoRadius);
    }
    
    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Triggering! : " + other.name);
        if (other.CompareTag("CameraFollow"))
        {
            if (isRotationNode)
            {
                //iTween.RotateTo(other.gameObject, new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z), 1.0f);
                iTween.RotateTo(other.gameObject, new Vector3(targetRotation.x, targetRotation.y, targetRotation.z), 2.0f);
                //Transform pivot = other.gameObject.transform.Find("Pivot");
                //iTween.MoveTo(pivot.gameObject, iTween.Hash("position", pivotPosition, "isLocal", true, "time", 2.0f));
            }
            /*
            if (isPositionNode)
            {
                Vector3[] smallPath = new Vector3[2];
                smallPath[0] = other.gameObject.transform.position;
                smallPath[1] = targetPosition;
                iTween.MoveTo(other.gameObject, iTween.Hash("path", smallPath, "speed", cameraFollowing.cameraSpeed, "orienttopath", true, "looktime", 2.0, "easetype", "linear"));
            }        

            if (isZoomNode)
            {
                cameraFollowing.ZoomTo(targetSize, 1.0f);
            }

            if (isSpeedNode)
            {
                cameraFollowing.cameraSpeed = targetSpeed;
            } 
             */ 
        }
    }
     
}
