﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class CameraPath
{
    public String pathID;
    public List<CameraNode> nodes;
}
