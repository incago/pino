﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public int HP = 5;
    public float moveForce = 10f;
    public float maxSpeed = 5f;
    public float animSpeedMultiplier = 1.0f;
    public Animator animator;

    void Awake()
    {
        animator.speed = animSpeedMultiplier;
    }

    void FixedUpdate()
    {
        GetComponent<Rigidbody>().AddForce(-moveForce, 0f, 0f);
        if (Mathf.Abs(GetComponent<Rigidbody>().velocity.x) > maxSpeed)
            GetComponent<Rigidbody>().velocity = new Vector3(Mathf.Sign(GetComponent<Rigidbody>().velocity.x) * maxSpeed, GetComponent<Rigidbody>().velocity.y, GetComponent<Rigidbody>().velocity.z);
    }

    void Update()
    {
        animator.SetBool("Grounded", true);
    }
}
