﻿using UnityEngine;
using System.Collections;

public enum CameraDirection
{
    DIRECTION_RIGHT, DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_UP
}

public enum Direction
{
    RIGHT, LEFT, UP, DOWN, STRAIGHT
}

public enum PathType
{
    Default, BeforeBranch, BeforeNextBlock
}

public enum BranchLocation
{
    BOTTOMRIGHT, BOTTOMLEFT, TOPRIGHT, TOPLEFT
}

public enum BranchType
{
    STRAIGHTANDLEFT, STRAIGHTANDRIGHT
}