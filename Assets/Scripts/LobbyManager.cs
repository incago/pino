﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class LobbyManager : MonoBehaviour 
{
    public GameObject stageButtonPrefab;

    public GameObject stagePanel;
    public Button playButton;

    private List<Stage> stageList = new List<Stage>();

    void Awake()
    {
        InitUI();
    }

    void InitUI()
    {
        InitButtons();
        InitStagePanel();
    }

    void InitButtons()
    {
        playButton.onClick.AddListener(() => { OnPlayClicked(); });
    }

    void InitStagePanel()
    {
        TextAsset stageListTextAsset = (TextAsset)Resources.Load("Json/stage_list");
        JObject stageListJObject = JObject.Parse(stageListTextAsset.text);
        stageList = JsonConvert.DeserializeObject<List<Stage>>(stageListJObject["stageList"].ToString());
        for (int stageIndex = 0; stageIndex < stageList.Count; stageIndex++)
            AddStageButtonItem(stageList[stageIndex], stagePanel);
        stagePanel.transform.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = 1.0f;
    }

    void AddStageButtonItem(Stage stage, GameObject stagePanel)
    {
        GameObject instance = (GameObject)Instantiate(stageButtonPrefab);
        instance.transform.SetParent(stagePanel.transform);
        instance.transform.localScale = Vector3.one;
        instance.GetComponent<RectTransform>().localPosition = Vector2.zero;
        instance.GetComponent<RectTransform>().sizeDelta = Vector2.zero;

        StageButton stageButton = instance.GetComponent<StageButton>();
        stageButton.stage = stage;
        stageButton.InitButton();
    }

    private void OnPlayClicked()
    {
        Util.Log("OnPlayClicked : stageId = " + StateManager.instance.stageId);

        StateManager.instance.StartGame();
        Application.LoadLevel("03_Stage");
        //Application.LoadLevel("04_Temp");
    }
}
