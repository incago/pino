﻿using UnityEngine;
using System.Collections;

public class Node : MonoBehaviour
{
    public Node prevNode;
    public Node nextNode;

    public void LinkOverlapNode()
    {
        
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.25f);
        int i = 0;

        while (i < hitColliders.Length)
        {
            if (hitColliders[i].GetComponent<Node>() != null && hitColliders[i].gameObject != gameObject)
            {
                if (prevNode == null)
                {
                    nextNode.prevNode = hitColliders[i].GetComponent<Node>();
                }
                else if (nextNode == null)
                {
                    prevNode.nextNode = hitColliders[i].GetComponent<Node>();
                }
                return;
            }
            i++;
        }
    }
}
