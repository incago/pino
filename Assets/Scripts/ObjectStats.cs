﻿using UnityEngine;
using System.Collections;

public class ObjectStats : MonoBehaviour, ITakeDamage
{
    public bool isInvincible { get; private set; }
    public bool isDead { get; set; }
    public int maxHP;
    public int currentHP;

    public virtual void TakeDamage(int damage, GameObject instigator)
    {

    }
}
