using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PinoController : MonoBehaviour
{
    private StageManager stageManager;
    private UIManager uiManager;
    private Pino pino;

    public PinoPath currentPath;
    public Transform[] controlPath;
    public Transform character;
    public Animator animator;

    public bool isDead = false;

    public float currentSpeed;
    public float normalSpeed = 1.5f;
    public float boosterSpeed = 2.5f;

    public float pathPosition = 0;
    public float speed = .2f;
    public float gravity = .5f;
    public float jumpForce = .12f;
    public float ySpeed = 0;

    private RaycastHit hit;
    private float rayLength = 5;
    private Vector3 floorPosition;
    private float lookAheadAmount = .01f;
        
    private uint jumpState = 0; //0=grounded 1=jumping
    private uint doubleJumpState = 0;
    private bool isBoostered = false;

    public ParticleSystem boosterParticle;
     
    void Awake()
    {
        stageManager = GameObject.FindGameObjectWithTag("StageManager").GetComponent<StageManager>();
        uiManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        pino = character.GetComponent<Pino>();
    }

    void Start()
    {
        speed = (10.0f / stageManager.currentBlock.pinoPath.pathLength) * currentSpeed;
        
        foreach (Transform child in character)
        {
            child.gameObject.layer = 2;
        }   
    }

    public void SetMoveSpeed(float _moveSpeed)
    {
        currentSpeed = _moveSpeed;
        pino.animator.speed = currentSpeed;
        speed = (10.0f / currentPath.pathLength) * currentSpeed;
    }

    

    void Update()
    {
        if (controlPath.Length > 1 && !isDead && !uiManager.isPaused)
        {
            DetectKeys();
            FindFloorAndRotation();
            MoveCharacter();
        }
    }


    void DetectKeys()
    {
        pathPosition += Time.deltaTime * speed;
        pino.animator.SetFloat("Speed", currentSpeed);
    }
    
    public void Jump()
    {
        if (jumpState == 0 && doubleJumpState == 0)
        {
            ySpeed -= jumpForce;
            jumpState = 1;
            pino.animator.SetBool("Grounded", false);
        }
        else if(jumpState == 1 && doubleJumpState == 0)
        {
            ySpeed -= jumpForce;
            doubleJumpState = 1;
        }
    }


    void FindFloorAndRotation()
    {
        if(pathPosition>= 1.0f)
        {
            if (currentPath.nextPath!= null)
            {
                stageManager.MakeNextStreetBlock();
                currentPath = currentPath.nextPath;
                controlPath = currentPath.data;
                SetMoveSpeed(currentSpeed);
                pathPosition = 0;
            }
            else
            {
                Debug.Log("no more path");
            }
        }
        float pathPercent = pathPosition;
        Vector3 coordinateOnPath = iTween.PointOnPath(controlPath, pathPercent);
        
        Vector3 lookTarget;

        if (pathPercent - lookAheadAmount >= 0 && pathPercent + lookAheadAmount <= 1)
        {
            lookTarget = iTween.PointOnPath(controlPath, pathPercent + lookAheadAmount);   
            character.LookAt(lookTarget);

            float yRot = character.eulerAngles.y;
            character.eulerAngles = new Vector3(0, yRot, 0);
        }
        
        if (Physics.Raycast(new Vector3(coordinateOnPath.x, coordinateOnPath.y, coordinateOnPath.z), -Vector3.up, out hit, rayLength, 1 << LayerMask.NameToLayer("Ground")))
        {
            Debug.DrawRay(new Vector3(coordinateOnPath.x, coordinateOnPath.y, coordinateOnPath.z), -Vector3.up * hit.distance);
            floorPosition = hit.point;
        }
    }


    void MoveCharacter()
    {
        ySpeed += gravity * Time.deltaTime;
        character.position = new Vector3(floorPosition.x, character.position.y - ySpeed, floorPosition.z);
        
        if (character.position.y < floorPosition.y)
        {
            ySpeed = 0;
            jumpState = 0;
            doubleJumpState = 0;
            pino.animator.SetBool("Grounded", true);
            character.position = new Vector3(floorPosition.x, floorPosition.y, floorPosition.z);
        }
    }

    public void Booster()
    {
        if(!isBoostered && !isDead && !StateManager.instance.isWaitStart)
        {
            float waitTime = 5.0f;
            StartCoroutine(BoosterCo(waitTime));
        }
    }
   
    public void KilledByVehicle(GameObject vehicle)
    {
        Debug.Log("Car Crash! : " + vehicle.name);
        isDead = true;
        Vector3 deltaPosition = vehicle.transform.position - transform.position;
        if (Mathf.Abs(deltaPosition.x) > 1.5f)
        {
            transform.localScale = new Vector3(1.0f, 1.0f, 0.1f);
            transform.parent = vehicle.transform;
            animator.speed = 0.0f;
        }
        else
        {
            transform.localScale = new Vector3(1.0f, 0.1f, 1.0f);
            animator.speed = 0.0f;
        }
        DeadProcess("Car Crash! : " + vehicle.name);
    }

    public void KilledByObstacle(GameObject obstacle)
    {
        Debug.Log("Obstacle Crash! : " + obstacle.name);
        isDead = true;        
        transform.localScale = new Vector3(1.0f, 1.0f, 0.1f);
        animator.speed = 0.0f;
        DeadProcess("Obstacle Crash! : " + obstacle.name);
    }
    
    IEnumerator BoosterCo(float waitTime)
    {
        bool currentInvincible = stageManager.uiManager.invincibleToggle.isOn;
        if (!currentInvincible) stageManager.uiManager.invincibleToggle.isOn = true;
        isBoostered = true;
        boosterParticle.Play();
        
        SetMoveSpeed(boosterSpeed);
        stageManager.cameraFollow.ZoomTo(15.0f, 0.5f);
        yield return new WaitForSeconds(waitTime);
        stageManager.cameraFollow.ZoomTo(12.0f, 0.5f);
        
        if (!isDead)
            SetMoveSpeed(normalSpeed);
        isBoostered = false;
        if (!currentInvincible) stageManager.uiManager.invincibleToggle.isOn = false;
    }

    public void SetJumpForce(Slider slider)
    {
        jumpForce = slider.value;
    }

    public void SetGravity(Slider slider)
    {
        gravity = slider.value;
    }

    public void SetNormalSpeed(Slider slider)
    {
        normalSpeed = slider.value;
        SetMoveSpeed(normalSpeed);
    }

    public void SetBoosterSpeed(Slider slider)
    {
        boosterSpeed = slider.value;
    }

    public void DeadProcess(string deadReason)
    {
        uiManager.gameOverAnimator.SetBool("isOn", true);
        uiManager.controlAnimator.SetBool("isDead", isDead);
        uiManager.deadReasonLabel.text = deadReason;
    }
}