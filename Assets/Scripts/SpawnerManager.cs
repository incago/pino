﻿using UnityEngine;
using System.Collections;

public class SpawnerManager : MonoBehaviour
{
    private Transform player;
    public GameObject enemyPrefab;
    public float distanceFromPlayer = 30.0f;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void FixedUpdate()
    {
        TrackPlayer();
    }

    void TrackPlayer()
    {
        float targetX = player.position.x;
        transform.position = new Vector3(targetX + distanceFromPlayer, transform.position.y, transform.position.z);
    }
}
