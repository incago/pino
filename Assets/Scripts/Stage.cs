﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Stage
{
    public string id;
    public string name;
    public string file;
	
    public Stage(string _id, string _name, string _file)
    {
        id = _id;
        name = _name;
        file = _file;
    }
}
