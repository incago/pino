﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 
/// </summary>
public class StateManager : MonoBehaviour {

    private static StateManager _instance;
    public static StateManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<StateManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    public string stageId= "1";
    public bool isWaitStart = true;

    public void StartGame()
    {
        Start();
    }

    void Start()
    {

    }

    public void EndGame()
    {
        End();
    }

    void End()
    {

    }

    public void Reset()
    {
        Time.timeScale = 1.0f;
        Application.LoadLevel("03_Stage");
    }

    public void Lobby()
    {
        Time.timeScale = 1.0f;
        Application.LoadLevel("02_Lobby");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void SetGraphicQuality(int level)
    {
        QualitySettings.SetQualityLevel(level);
    }
}
