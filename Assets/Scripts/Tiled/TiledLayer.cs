﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class TiledLayer
{
    public string name;
    public string type;
    public string draworder;
    
    public int[] data;
    public List<TiledObject> objects;

    public int height;
    public int width;
    public int x;
    public int y;

    public float opacity;
    
    public bool visible;

    public TiledLayerProperties properties; 
}
