﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class TiledLayerProperties
{
    public float startPositionX;
    public float startPositionY;
    public float startPositionZ;

    public float startRotationX;
    public float startRotationY;
    public float startRotationZ;

    public float startPivotX;
    public float startPivotY;
}
