﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class TiledMap
{
    public string renderorder;
    public string orientation; 
    
    public int height;
    public int width;
    public int tilewidth;
    public int tileheight;
    public int version;
    public int nextobjectid;
    
    public List<TiledLayer> layers;
    public List<TiledTileset> tilesets;
}
