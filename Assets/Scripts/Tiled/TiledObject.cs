﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class TiledObject
{
    public int gid;
    public int height;
    public int id;
    public string name;
    public int rotation;
    public string type;
    public bool visible;
    public int width;
    public int x;
    public int y;
    public Vector2[] polyline;
    public TiledObjectProperties properties;
}
