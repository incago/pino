﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class TiledObjectProperties
{
    public bool isSpeedNode;
    public float targetSpeed;
    
    public bool isRotationNode;
    public float targetRotationX;
    public float targetRotationY;
    public float targetRotationZ;
    public float pivotPositionX;
    public float pivotPositionY;

    public bool isZoomNode;
    public float targetSize;

    public bool isStartBlock;
    public bool isEndBlock;

    public string prefabName;

    public string blockDirection;
    public string blockType;
}
