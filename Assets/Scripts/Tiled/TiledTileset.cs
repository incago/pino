﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class TiledTileset
{
    public string name;
    public string image;

    public int firstgid;
    public int imageheight;
    public int imagewidth;
    public int margin;
    public int spacing;
    public int tileheight;
    public int tilewidth;
}
