﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util : ScriptableObject
{
    private static void JavascriptLog(string msg)
    {
        Application.ExternalCall("console.log", msg);
    }

    public static void Log(string message)
    {
        Debug.Log(message);
        if (Application.isWebPlayer)
            JavascriptLog(message);
    }

    public static void LogError(string message)
    {
        Debug.LogError(message);
        if (Application.isWebPlayer)
            JavascriptLog(message);
    }
}
