﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class StageButton : MonoBehaviour, IPointerClickHandler 
{
    [SerializeField]
    public Stage stage;
    public Text stageLabel;
    public Image stageThumbnail;

    public void InitButton()
    {
        stageLabel.text = stage.name;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        StateManager.instance.stageId = this.stage.id;
    }
}
