﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class StageManager : MonoBehaviour 
{
    public PinoController pinoController;
    public Pino pino;
    public CameraFollow cameraFollow;
    public UIManager uiManager;
    
    public GameObject straightStreetPrefab, leftTurnStreetPrefab, rightTurnStreetPrefab;
    public GameObject pinoPrefab;
    public GameObject[] vehiclePrefabs;
    public GameObject[] buildingBlockPrefabs;

    public Block beforeBlock, currentBlock, nextBlock, nextNextBlock;

    private TiledMap tiledMap;
    private TiledLayer pathLayer;
    private int nextMakeBlockIndex = 0;
    
    void Awake()
    {
        QualitySettings.SetQualityLevel(1);
        InitUI();
        ReadStage();

        MakeNextStreetBlock();
        MakeNextStreetBlock();
        MakeNextStreetBlock();
        MakeNextStreetBlock();

        SpawnPino();
    }

    void Start()
    {
        cameraFollow.StartCameraWork(3.0f);
        cameraFollow.SetCameraBlur(false);
        uiManager.SetPlayUIVisible(true);
        uiManager.Count();
        uiManager.TrackDistance();
    }

    void InitUI()
    {
        
    }

    void ReadStage()
    {
        TextAsset stageTextAsset = (TextAsset)Resources.Load("Json/stage_" + StateManager.instance.stageId);
        JObject stageListJObject = JObject.Parse(stageTextAsset.text);
        tiledMap = JsonConvert.DeserializeObject<TiledMap>(stageListJObject.ToString());

        for (int layerIndex = 0; layerIndex < tiledMap.layers.Count; layerIndex++)
        {
            if (tiledMap.layers[layerIndex].name.Equals("Path"))
                pathLayer = tiledMap.layers[layerIndex];
        }
    }

    Block MakeStreet(int blockIndex)
    {
        TiledObjectProperties streetProperties = pathLayer.objects[blockIndex].properties;
        GameObject targetStreet = straightStreetPrefab;
        if (streetProperties.blockType.Equals("straight")) targetStreet = straightStreetPrefab;
        else if (streetProperties.blockType.Equals("leftTurn")) targetStreet = leftTurnStreetPrefab;
        else if (streetProperties.blockType.Equals("rightTurn")) targetStreet = rightTurnStreetPrefab;

        Vector3 streetPosition = new Vector3(pathLayer.objects[blockIndex].x, 0, -pathLayer.objects[blockIndex].y);

        Quaternion streetRotation = Quaternion.identity;
        if (streetProperties.blockDirection.Equals("right")) streetRotation = Quaternion.identity;
        else if (streetProperties.blockDirection.Equals("up")) streetRotation = Quaternion.Euler(new Vector3(0, -90.0f, 0));
        else if (streetProperties.blockDirection.Equals("left")) streetRotation = Quaternion.Euler(new Vector3(0, -180.0f, 0));
        else if (streetProperties.blockDirection.Equals("down")) streetRotation = Quaternion.Euler(new Vector3(0, -270.0f, 0));

        GameObject streetGameObject = targetStreet.Spawn(streetPosition, streetRotation);
        streetGameObject.GetComponent<Block>().isVehicleActive = true;
        
        Block block = streetGameObject.GetComponent<Block>();
        if (!streetProperties.blockType.Equals("straight"))
        {
            block.cameraTrigger.isRotationTrigger = true;
            block.cameraTrigger.direction = streetProperties.blockDirection;
        }

        return block;
    }

    public void MakeNextStreetBlock()
    {
        if (nextMakeBlockIndex < pathLayer.objects.Count)
        {
            if (beforeBlock != null)
            {
                beforeBlock.isVehicleActive = false;
                beforeBlock.gameObject.Recycle();
            }
            beforeBlock = currentBlock;
            currentBlock = nextBlock;
            nextBlock = nextNextBlock;
            nextNextBlock = MakeStreet(nextMakeBlockIndex++);
            if (nextBlock != null)
                nextBlock.pinoPath.nextPath = nextNextBlock.pinoPath;
        }
        else
        {
            Debug.Log("No more block to make");
        }
    }

    void SpawnPino()
    {
        Vector3 spawnPosition = new Vector3(currentBlock.pinoPath.data[0].transform.position.x, 0, currentBlock.pinoPath.data[0].transform.position.z);
        GameObject pinoGameObject = Instantiate(pinoPrefab, spawnPosition, Quaternion.Euler(new Vector3(0, 90.0f, 0))) as GameObject;

        pinoController = pinoGameObject.GetComponent<PinoController>();
        pino= pinoGameObject.GetComponent<Pino>();
        pinoController.currentPath= currentBlock.pinoPath;
        pinoController.controlPath = currentBlock.pinoPath.data;
    }
        
    public void SetJumpForce(Slider slider)
    {
       pinoController.jumpForce = slider.value;
    }

    public void SetGravity(Slider slider)
    {
        pinoController.gravity = slider.value;
    }

    public void SetNormalSpeed(Slider slider)
    {
        pinoController.normalSpeed = slider.value;
        pinoController.SetMoveSpeed(pinoController.normalSpeed);
    }

    public void SetBoosterSpeed(Slider slider)
    {
        pinoController.boosterSpeed = slider.value;
    }

    public void ToggleInvincible(Toggle toggle)
    {
        pino.isInvincible = toggle.isOn;
    }

    public void Reset()
    {
        StateManager.instance.Reset();
    }

    public void Lobby()
    {
        StateManager.instance.Lobby();
    }

    public void Quit()
    {
        StateManager.instance.Quit();
    }

    public void Pause()
    {
        uiManager.Pause();
    }

    public void SetGraphicQuality(int level)
    {
        StateManager.instance.SetGraphicQuality(level);
    }
}
