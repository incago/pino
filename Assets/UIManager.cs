﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using Consolation;

public class UIManager : MonoBehaviour 
{
    private CameraFollow cameraFollow;
    private StageManager stageManager;
    public HorizontalMoveButton leftMoveButton, rightMoveButton;
    public BoosterButton boosterButton;
    public JumpButton jumpButton;
    public Toggle invincibleToggle;
    public Text countLabel, distanceLabel, deadReasonLabel;

    public GameObject controlGroup, pauseGroup, gameOverGroup, scoreGroup;
    public Animator controlAnimator, pauseAnimator, gameOverAnimator, scoreAnimator;

    public bool isPaused = false;
    public bool isPauseOffIng = false;
    public bool isConsole = false;
    private StringBuilder sb = new StringBuilder();

    public Slider jumpForceSlider, gravitySlider, normalSpeedSlider, boosterSpeedSlider;

    void Awake()
    {
        stageManager = GameObject.FindGameObjectWithTag("StageManager").GetComponent<StageManager>();
        cameraFollow = GameObject.FindGameObjectWithTag("CameraFollow").GetComponent<CameraFollow>();
        pauseAnimator.SetBool("isPaused", false);
    }

    void Start()
    {
        //SetLobbyUIVisible(true);
        pauseGroup.GetComponent<CanvasGroup>().interactable = false;    
    }

    public void SetLobbyUIVisible(bool _active)
    {
        //lobbyGroup.SetActive(_active);
        //lobbyGroup.GetComponent<CanvasGroup>().interactable = _active;
        controlGroup.SetActive(!_active);
        controlGroup.GetComponent<CanvasGroup>().interactable = !_active;
    }

    public void SetPlayUIVisible(bool _active)
    {
        controlGroup.SetActive(_active);
        controlGroup.GetComponent<CanvasGroup>().interactable = _active;
        //lobbyGroup.SetActive(!_active);
        //lobbyGroup.GetComponent<CanvasGroup>().interactable = !_active;
    }

    public void Pause()
    {
        if (!isPauseOffIng)
        {
            if (!isPaused)
            {
                isPaused = true;
                Time.timeScale = 0.0f;
                pauseAnimator.SetBool("isPaused", isPaused);
            }
            else if(isPaused)
            {
                StartCoroutine(PauseOffCo());
            }
        }
    }

    IEnumerator PauseOffCo()
    {
        isPauseOffIng = true;
        isPaused = false;
        pauseAnimator.SetBool("isPaused", isPaused);
        Time.timeScale = 0.1f;
        while(Time.timeScale < 1.0f)
        {
            Time.timeScale += Time.deltaTime * 1.0f;
            yield return null;
        }
        isPauseOffIng = false;
    }

    public void SetPauseUIVisible(bool _active)
    {
        pauseGroup.SetActive(_active);
        pauseGroup.GetComponent<CanvasGroup>().interactable = _active;
        controlGroup.SetActive(!_active);
        controlGroup.GetComponent<CanvasGroup>().interactable = !_active;
    }

    public void SetCameraBlur(bool _active)
    {
        cameraFollow.SetCameraBlur(_active);
    }

    public void Booster()
    {
        stageManager.pinoController.Booster();
    }
    
    public void Count()
    {
        StartCoroutine(CountCo());
    }

    IEnumerator CountCo()
    {
        countLabel.text = "3";
        yield return new WaitForSeconds(1.0f);
        countLabel.text = "2";
        yield return new WaitForSeconds(1.0f);
        countLabel.text = "1";
        yield return new WaitForSeconds(1.0f);
        countLabel.text = "Commute Start!";
        invincibleToggle.isOn = false;
        StateManager.instance.isWaitStart = false;
        stageManager.pino.isInvincible = false;
        stageManager.pinoController.SetMoveSpeed(stageManager.pinoController.normalSpeed);

        yield return new WaitForSeconds(2.0f);
        countLabel.text = "";
    }

    public void TrackDistance()
    {
        StartCoroutine(TrackDistanceCo());
    }

    IEnumerator TrackDistanceCo()
    {
        float distance = 0;
        while (!stageManager.pinoController.isDead)
        {
            sb.Length = 0;
            if (!isPaused && !isPauseOffIng)
                distance += stageManager.pinoController.currentSpeed / 10;
            sb.Append(distance.ToString("F0"));
            sb.Append("m");
            distanceLabel.text = sb.ToString();
            yield return null;
        }
    }

    public void ToggleConsole()
    {
        isConsole = !isConsole;
    }
}
