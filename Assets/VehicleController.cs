﻿using UnityEngine;
using System.Collections;

public class VehicleController : MonoBehaviour 
{
    public Block parentBlock;
    public VehiclePath vehiclePath;
    public Transform[] controlPath;

    public float moveSpeed;
    public float pathPosition = 0;
    public float speed = .2f;

    private Vector3 floorPosition;
    private float lookAheadAmount = .01f;
    private Transform vehicleTransform;

    void Awake()
    {
        vehicleTransform = transform;
    }

    public void Initialize(Block _parentBlock, VehiclePath _vehiclePath, float _pathPosition, float _moveSpeed)
    {
        parentBlock = _parentBlock;
        vehiclePath = _vehiclePath;
        vehiclePath.Initialize();
        controlPath = vehiclePath.data;
        pathPosition = _pathPosition;
        moveSpeed = _moveSpeed;
        speed = (10.0f / vehiclePath.pathLength) * moveSpeed;
    }

    void Update()
    {
        if (parentBlock.isVehicleActive && controlPath.Length > 1)
        {
            pathPosition += Time.deltaTime * speed;
            FindFloorAndRotation();
            MoveVehicle();
        }
    }

    void FindFloorAndRotation()
    {
        float pathPercent = pathPosition % 1;
        Vector3 lookTarget;
        if (pathPercent - lookAheadAmount >= 0 && pathPercent + lookAheadAmount <= 1)
        {
            lookTarget = iTween.PointOnPath(controlPath, pathPercent + lookAheadAmount);
            vehicleTransform.LookAt(lookTarget);
            float yRot = vehicleTransform.eulerAngles.y;
            vehicleTransform.eulerAngles = new Vector3(0, yRot, 0);
        }
        floorPosition = iTween.PointOnPath(controlPath, pathPercent);
    }

    void MoveVehicle()
    {
        vehicleTransform.position = new Vector3(floorPosition.x, 0, floorPosition.z);
    }
}
