﻿using UnityEngine;
using System.Collections;

public class VehiclePath : MonoBehaviour
{
    public Transform[] data;
    public float pathLength;

    public void Initialize()
    {
        pathLength = iTween.PathLength(data);
    }

    void OnDrawGizmos()
    {
        if (data != null && data.Length > 1)
        {
            iTween.DrawPathGizmos(data, Color.red);
        }
    }
}
